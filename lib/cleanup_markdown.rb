# frozen_string_literal: true

# Handles cleaning up all markdown files in current workspace
class CleanupMarkdown
  class << self
    # Convert HTML image to markdown equivalent
    def convert_image_to_md(lines, line, index)
      print '.'
      # This line has link to convert
      first = (line.index('src="') + 5)
      src = line[first..(line.index('"', first) - 1)]
      src = src[0..(src.index('?') - 1)] if src.include? '?'
      new_image = "![Image](/.#{src})"
      lines[index] = new_image
    end

    # Convert all HTML image reference in file to markdown
    # @note Currently this is made to work based on a ADO wiki
    # @param [String] filename Name of file to convert
    def convert_images(filename)
      lines = File.read(filename).split("\n")

      puts "Processing #{lines.count} lines"
      lines.each_with_index do |line, index|
        line = line.lstrip
        next unless line.start_with? '<img src='

        convert_image_to_md(lines, line, index)
      end
      puts "\n"
      file_content = lines.join("\n")
      File.write filename, file_content
    end

    # @return [Array] Number of markdown files
    def markdown_files(markdown_folder)
      pattern = "#{markdown_folder}/**/*.md"
      puts "Looking for files matching #{pattern}"
      Dir.glob(pattern)
    end

    # Process all markdown files in current directory
    def process_files(markdown_folder = 'markdown')
      markdown_files(markdown_folder).each do |filename|
        puts "Processing #{filename}"
        convert_images filename
      end
    end
  end
end
