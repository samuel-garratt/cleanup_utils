# frozen_string_literal: true

require 'open3' # Used for handling stdout
require 'nokogiri'

# Override xml element so it knows how to compare 2 nodes
module Nokogiri
  module XML
    # Updated Nokogiri element to be able to easily perform similarity checks
    class Element
      # Return true if this node is content-equivalent to other, false otherwise
      def =~(other)
        return true if self == other
        return false unless name == other.name
        return false unless text == other.text

        stype = node_type; otype = other.node_type
        return false unless stype == otype

        sa = attributes; oa = other.attributes
        return false unless sa.length == oa.length

        value_sa = sa.values.collect(&:to_s).sort
        value_oa = oa.values.collect(&:to_s).sort
        sa.keys.sort == oa.keys.sort && value_sa == value_oa
      end
    end
  end
end

# Makes accessing results of commands via command line easy
class CmdLine
  # Easy access to status of command line run
  attr_reader :stdout, :stderr, :status

  # @param [String] command_line_to_run Command to run on terminal
  # @param [String] options Open3 options, E.g, chdir to execute within a folder
  def initialize(command_line_to_run, options)
    @name = command_line_to_run
    @stdout, @stderr, @status = Open3.capture3(command_line_to_run, options)
  end

  # @return [String] Result of executing Terminal instruction with STDOUT, STDERR, and Status
  def result
    stdout + stderr + status.to_s
  end

  # Output as array
  def lines
    stdout.lines
  end

  # Return name of command line typed
  def to_s
    @name
  end
end

# Clean up git changes that are irrelevant to an actual change and hence clog up space
class CleanUpUneededGitDiff
  class << self
    # @param [Array] diff_lines Lines for the diff of a file
    # @return [Boolean] Whether there are just 2 lines of XML that are the same apart from attribute order
    def xml_nodes_similar?(diff_lines)
      raise ArgumentError, "Parameter must be Array of lines not #{diff_lines.class}" unless diff_lines.is_a? Array

      old_lines = diff_lines.find_all { |line| line.strip.start_with? '-' }
      new_lines = diff_lines.find_all { |line| line.strip.start_with? '+' }
      return false if old_lines.count != new_lines.count # Indicate additions/removals

      old_lines[1..-1].each_with_index do |old_line, index|
        return false unless diff_is_equivalent(old_line, new_lines[index + 1])
      end
      true
    end

    # @return [Boolean] Whether git diff is equivalent
    def diff_is_equivalent(old_line, new_line)
      return false unless old_line # Ignore null lines

      old_xml_tag = old_line.strip[1..-1]
      new_xml_tag = new_line.strip[1..-1]
      # If text is similar but just closing tag
      return true if old_xml_tag == new_xml_tag && old_xml_tag.start_with?('<') && old_xml_tag.end_with?('>')
      return false if Nokogiri::XML(old_xml_tag).children.size.zero?

      return false if Nokogiri::XML(old_xml_tag).children.size > 1 # More than 1 child, don't touch

      old_xml_node = Nokogiri::XML(old_xml_tag).children[0]
      new_xml_node = Nokogiri::XML(new_xml_tag).children[0]
      old_xml_node =~ new_xml_node
    end

    # Remove git diffs that are simply one line of XML that is actually equivalent (attributes are just different order)
    def remove_xml_diff(folder)
      # TODO: Following 2 lines for efficiency
      # Run 'git --no-pager diff'
      # Parse output, split based on files. Grab all changes lines

      modified_files = CmdLine.new('git status', chdir: folder).lines.find_all { |l| l.include? 'modified:' }.collect { |l| l.split(' ')[1] }
      differences_to_undo = []
      puts "Analysing #{modified_files.count} modified files"
      modified_files.each do |filename|
        diff_lines = CmdLine.new("git diff #{filename}", chdir: folder).lines
        next unless xml_nodes_similar?(diff_lines)

        differences_to_undo << filename
        puts "Undoing changes to #{filename}"
      end
      CmdLine.new("git checkout -- #{differences_to_undo.join(' ')}", chdir: folder)
      puts "Undid changes to #{differences_to_undo.count} files"
    end
  end
end
