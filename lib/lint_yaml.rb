# frozen_string_literal: true

require 'yaml'

# Lints YAML files
class LintYaml
  # Error on linting
  class LintError < StandardError; end

  # @return [Array] Number of markdown errors
  @errors = []
  class << self
    # Parsing errors
    attr_reader :errors

    def enforce_camel_case_for(hash)
      hash.each do |key, value|
        if /[[:upper:]]/.match(key[0].to_s)
          @errors << key
          puts "  #{key} is not correctly camel case"
        end
        enforce_camel_case_for value if value.is_a? Hash
      end
    end

    def enforce_camel_case_file(filename)    
      yaml = YAML.load_file filename
      enforce_camel_case_for yaml
    rescue Psych::SyntaxError
      puts " ** Cannot parse #{filename} due to syntax"
    end

    # @return [Array] Number of markdown files
    def yaml_files(folder)
      base_pattern = File.join("**", "*.yaml")
      pattern = folder ? File.join(folder, base_pattern) : base_pattern   
      puts "Looking for files matching #{pattern}"
      Dir.glob(pattern)
    end

    # Process all YAML files in current directory
    def process_files(folder = nil)
      yaml_files(folder).each do |filename|
        puts "Processing #{filename}"
        enforce_camel_case_file filename
      end
      raise LintError, "Found #{@errors.count} errors" if @errors.count.positive?
    end
  end
end
