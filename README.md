This repo contains a series of scripts to cleanup certain directories.

# Converting markdown

Example of running cleanup of markdown directory

`docker run --rm -v <markdownRepo>:/app/markdown registry.gitlab.com/samuel-garratt/cleanup_utils markdown`

where `<markdownRepo>` is the location of the markdown repo to perform conversion on

# Removing 1 line xml diffs that are not actually different

This command will remove 1 line xml diffs where only the attributes of the XML are rearranged.

`docker run --rm -v <pathToGitRepo>:/app/repo registry.gitlab.com/samuel-garratt/cleanup_utils git_xml`

where `<pathToGitRepo>` is the path to the git repo (on linux or windows powershell this can be the current directory with `$(pwd)`. On windows cmd %cd%). See [here](https://stackoverflow.com/a/41489151/4696083) for explanation of using current working directory in docker.

So from the git repo

`docker run --rm -v $(pwd):/app/repo registry.gitlab.com/samuel-garratt/cleanup_utils git_xml`

or on Windows cmd

`docker run --rm -v %cd%:/app/repo registry.gitlab.com/samuel-garratt/cleanup_utils git_xml`

