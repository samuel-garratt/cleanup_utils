FROM ruby:2.6

RUN mkdir /app
WORKDIR /app
RUN mkdir repo
COPY . /app
RUN gem install bundler
RUN bundle install --without develop
ENV DOCKER true

ENTRYPOINT ["ruby", "exe/cleanup_utils"]
CMD ["help"]
