# frozen_string_literal: true

require_relative '../lib/cleanup_unneeded_git_diff'

describe Nokogiri::XML do
  context '=~' do
    it 'not true for diff value' do
      a = Nokogiri::XML('<x>A</x>').children[0]
      b = Nokogiri::XML('<x>B</x>').children[0]
      expect(a =~ b).to be false
    end
    it 'not true for diff attribute' do
      a = Nokogiri::XML('<x a="1">A</x>').children[0]
      b = Nokogiri::XML('<x a="2">A</x>').children[0]
      expect(a =~ b).to_not be true
    end
    it 'true for reordered attribute' do
      a = Nokogiri::XML('<x a="1" b="2">A</x>').children[0]
      b = Nokogiri::XML('<x b="2" a="1">A</x>').children[0]
      expect(a =~ b).to be true
    end
  end
end

describe CleanUpUneededGitDiff do
  let(:git_2_xml_nodes_same) do
    '''diff --git a/Users-API-Old-%28Do-not-use%29/%2Fv1%2FPing.xml b/Users-API-Old-%28Do-not-use%29/%2Fv1%2FPing.xml
index 74ad208..cfd67b7 100644
--- a/Users-API-Old-%28Do-not-use%29/%2Fv1%2FPing.xml
+++ b/Users-API-Old-%28Do-not-use%29/%2Fv1%2FPing.xml
@@ -1,5 +1,5 @@
  <?xml version="1.0" encoding="UTF-8"?>
-<con:resource name="/v1/Ping" path="/v1/Ping" id="2b68e138-a230-4486-9ada-f2ef1053b6be" xmlns:con="http://eviware.com/soapui/config" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
+<con:resource name="/v1/Ping" path="/v1/Ping" id="2b68e138-a230-4486-9ada-f2ef1053b6be" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:con="http://eviware.com/soapui/config">
    <con:settings>
      <con:setting id="2b68e138-a230-4486-9ada-f2ef1053b6befileName">%2Fv1%2FPing</con:setting>
    </con:settings>
        '''
  end
  let(:more_than_1_xml_diff) do
    '''diff --git a/spec/eg.xml b/spec/eg.xml
index 9f0c5ff..4e56657 100644
--- a/spec/eg.xml
+++ b/spec/eg.xml
@@ -1,5 +1,5 @@
 <example>
   <valid value="from this">Test</valid>
   <p>Some other change</p>
-  <reorder b="a" a="b">Test</reorder>
-</example>
+  <reorder a="b" b="a">Test</reorder>
+</example>
    '''
  end
  let(:mix_of_valid_invalid_diff) do
    '''diff --git a/spec/eg.xml b/spec/eg.xml
index 9f0c5ff..4e56657 100644
--- a/spec/eg.xml
+++ b/spec/eg.xml
@@ -1,5 +1,5 @@
 <example>
   <valid value="from this">Test</valid>
   <p>Some other change</p>
-  <reorder b="a" a="b">Test</reorder>
-<example close="1">
+  <reorder a="b" b="a">Test</reorder>
+<example close="2">
    '''
  end
  let(:adding_line) do
    '''diff --git a/spec/unconverted.md b/spec/unconverted.md
index 266c1a9..796ab96 100644
--- a/spec/unconverted.md
+++ b/spec/unconverted.md
@@ -7,3 +7,4 @@
 2nd image

 <img src="attachments/183566337/345145361.png" class="image-center" />
+Other text
    '''
  end
  it 'can perform removal of diff' do
    expect(CleanUpUneededGitDiff).to respond_to :remove_xml_diff
  end
  it 'consider 2 xml nodes with same attributes =~' do
    git_diff_lines = git_2_xml_nodes_same.lines
    expect(CleanUpUneededGitDiff.xml_nodes_similar?(git_diff_lines)).to be true
  end
  it 'handles more than 1 difference when XML =~ is same' do
    git_diff_lines = more_than_1_xml_diff.lines
    expect(CleanUpUneededGitDiff.xml_nodes_similar?(git_diff_lines)).to be true
  end
  it 'does not affect mix of invalid/valid diffs' do
    git_diff_lines = mix_of_valid_invalid_diff.lines
    expect(CleanUpUneededGitDiff.xml_nodes_similar?(git_diff_lines)).to be false
  end
  it 'does not affect mix of invalid/valid diffs' do
    git_diff = adding_line.lines
    expect(CleanUpUneededGitDiff.xml_nodes_similar?(git_diff)).to be false
  end

  it 'gracefully handles incorrect input' do
    expect do
      CleanUpUneededGitDiff.xml_nodes_similar?('lines')
    end.to raise_exception ArgumentError
  end
  it 'does not affect valid diff' do
    valid_diff = '''
diff --git a/Users-API-Old-%28Do-not-use%29/element.order b/Users-API-Old-%28Do-not-use%29/element.order
index 7512042..be8c0c2 100644
--- a/Users-API-Old-%28Do-not-use%29/element.order
+++ b/Users-API-Old-%28Do-not-use%29/element.order
@@ -1,11 +1,11 @@
  API1
+Reordered-API
  API2
-Reordered-API
  API3
    '''.lines
    expect(CleanUpUneededGitDiff.xml_nodes_similar?(valid_diff)).to be false
  end
end
