# frozen_string_literal: true

require_relative '../lib/cleanup_markdown'
describe CleanupMarkdown do
  it 'finds markdown files' do
    expect(CleanupMarkdown.markdown_files('spec').count).to eq 1
  end
end
