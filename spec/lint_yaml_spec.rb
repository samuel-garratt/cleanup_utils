# frozen_string_literal: true

require_relative '../lib/lint_yaml'
describe LintYaml do
  it 'finds YAML files' do
    expect(LintYaml.yaml_files('spec').count).to eq 1
  end
  it 'finds camelcase errors' do
    expect do
      LintYaml.process_files('spec')
    end.to raise_error LintYaml::LintError, /2/
    expect(LintYaml.errors.count).to be 2
  end
end
